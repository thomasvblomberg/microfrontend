import {
  constructRoutes,
  constructApplications,
  constructLayoutEngine,
} from "single-spa-layout";
import { registerApplication, start } from "single-spa";

const routes = constructRoutes(document.querySelector("#single-spa-layout"));

const applications = constructApplications({
  routes,
  loadApp: ({ name }) => System.import(name),
});

const layoutEngine = constructLayoutEngine({
  routes,
  applications,
  active: false,
});

applications.forEach(registerApplication);

const currentApps = JSON.parse(localStorage.getItem("APPS") || "[]");

currentApps.forEach(({ id, appName: name, appUrl }) =>
  registerApplication({
    name,
    app: () => System.import(appUrl),
    activeWhen: [`/${id}`],
  })
);

layoutEngine.activate();

start();
