let funcs = [];

export function subscribe(func: (msg: string) => void) {
  funcs = [...funcs, func];
}

export function publish(msg: string) {
  funcs.forEach((f) => f(msg));
}
