import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';

export default function Root() {
  const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  return (
		<main>
			<Container sx={{ py: 8 }} maxWidth='md'>
				<Grid container spacing={4}>
					{cards.map((card) => (
						<Grid item key={card} xs={12} sm={6} md={4}>
							<Card
								sx={{
									height: '100%',
									display: 'flex',
									flexDirection: 'column',
								}}
							>
								<CardMedia
									component='img'
									sx={{
										// 16:9
										pt: '56.25%',
									}}
									image='https://cataas.com/cat/gif'
									alt='random'
								/>
								<CardContent sx={{ flexGrow: 1 }}>
									<Typography
										gutterBottom
										variant='h5'
										component='h2'
									>
										KITTY
									</Typography>
									<Typography>Meow, meow, meow..</Typography>
								</CardContent>
								<CardActions>
									<Button size='small'>View</Button>
									<Button size='small'>Edit</Button>
								</CardActions>
							</Card>
						</Grid>
					))}
				</Grid>
			</Container>
		</main>
  );
}
