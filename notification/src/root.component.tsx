import { Snackbar } from "@mui/material";
import { useState } from "react";
import { subscribe } from "@bifrost/message";

export default function Root(props) {
  const [msg, setMessage] = useState("");

  const handleClose = () => setMessage(null);

  subscribe(msg => setMessage(msg));

  return (
		<Snackbar
			open={typeof msg !== 'undefined' && msg !== null}
			autoHideDuration={6000}
			onClose={handleClose}
			message={msg}
		/>
  );
}
