import AppBar from '@mui/material/AppBar';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Button from '@mui/material/Button';
const theme = createTheme();

export default function Root({ singleSpa }) {
  const { navigateToUrl } = singleSpa;

  return (
		<ThemeProvider theme={theme}>
			<CssBaseline />
			<AppBar position='sticky'>
				<Toolbar>
					<Typography variant='h6' color='inherit' noWrap>
						Bifröst
					</Typography>
					<nav>
						<Button
							color='inherit'
							style={{
								marginLeft: '15px',
							}}
							onClick={() => navigateToUrl('/apps')}
						>
							Apps
						</Button>
						<Button
							color='inherit'
							style={{
								marginLeft: '15px',
							}}
							onClick={() => navigateToUrl('/register')}
						>
							Register
						</Button>
					</nav>
				</Toolbar>
			</AppBar>
		</ThemeProvider>
  );
}