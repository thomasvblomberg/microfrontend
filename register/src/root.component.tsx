import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import { useForm, Controller } from 'react-hook-form';
import { publish } from '@bifrost/message';

export default function Root({ singleSpa }) {
	const { handleSubmit, control } = useForm();
	const { registerApplication, navigateToUrl } = singleSpa;

	const onSubmit = (formData: any) => {
		const { id, appName: name, appUrl } = formData;
		const key = "APPS";
		const currentApps = JSON.parse(localStorage.getItem(key) || "[]");

		registerApplication({
			name,
			app: () => System.import(appUrl),
			activeWhen: [`/${id}`],
		});

		const updatedApps = currentApps.filter(x => x.id !== id);

		localStorage.setItem(key, JSON.stringify([...updatedApps, formData]));

		publish(`${name} was created!`);

		navigateToUrl('/apps');
	};

	return (
		<main>
			<Box
				sx={{
					bgcolor: 'background.paper',
					pt: 8,
					pb: 6,
				}}
			>
				<Container maxWidth='sm'>
					<Typography variant='h6' gutterBottom>
						Application
					</Typography>
					<Grid container spacing={3}>
						<Grid item xs={12} sm={6}>
							<Controller
								name={'id'}
								control={control}
								render={({ field: { onChange, value } }) => (
									<TextField
										required
										label='App ID'
										fullWidth
										variant='standard'
										value={value}
										onChange={onChange}
									/>
								)}
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							<Controller
								name={'appName'}
								control={control}
								render={({ field: { onChange, value } }) => (
									<TextField
										required
										label='App name'
										fullWidth
										autoComplete='app-name'
										variant='standard'
										value={value}
										onChange={onChange}
									/>
								)}
							/>
						</Grid>
						<Grid item xs={12}>
							<Controller
								name={'appImgUrl'}
								control={control}
								render={({ field: { onChange, value } }) => (
									<TextField
										required
										label='App Image Url'
										fullWidth
										variant='standard'
										value={value}
										onChange={onChange}
									/>
								)}
							/>
						</Grid>
						<Grid item xs={12}>
							<Controller
								name={'appUrl'}
								control={control}
								render={({ field: { onChange, value } }) => (
									<TextField
										required
										label='App Url'
										fullWidth
										variant='standard'
										value={value}
										onChange={onChange}
									/>
								)}
							/>
						</Grid>
						<Grid item xs={12}>
							<Button
								variant='contained'
								onClick={handleSubmit(onSubmit)}
							>
								Register Application
							</Button>
						</Grid>
					</Grid>
				</Container>
			</Box>
		</main>
	);
}
