import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { useState } from 'react';
import { useEffect } from 'react';

export default function Root({ singleSpa }) {
  const { navigateToUrl } = singleSpa;
  const [apps, setApps] = useState([]);

  useEffect(() => {
    const currentApps = JSON.parse(localStorage.getItem('APPS') || '[]');
    setApps(currentApps);
  }, []);

	return (
		<main>
			<Container sx={{ py: 8 }} maxWidth='md'>
				{/* End hero unit */}
				<Grid container spacing={4}>
					{apps.length === 0 && (
            
						<Typography gutterBottom variant='h2' component='h2'>
							No Apps Yo!
						</Typography>
					)}
					{apps.map(({ id, appName: name, appImgUrl }) => (
						<Grid item key={id} xs={12} sm={6} md={4}>
							<Card
								sx={{
									height: '100%',
									display: 'flex',
									flexDirection: 'column',
								}}
							>
								<CardMedia
									component='img'
									sx={{
										// 16:9
										pt: '56.25%',
									}}
									image={appImgUrl}
									alt={name}
								/>
								<CardContent sx={{ flexGrow: 1 }}>
									<Typography
										gutterBottom
										variant='h5'
										component='h2'
									>
										{name}
									</Typography>
								</CardContent>
								<CardActions>
									<Button
										size='small'
										onClick={() => navigateToUrl(id)}
									>
										View
									</Button>
								</CardActions>
							</Card>
						</Grid>
					))}
				</Grid>
			</Container>
		</main>
	);
}
